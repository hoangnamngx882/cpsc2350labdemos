import express from 'express';
import request from 'supertest';

import sumFn from './sum.fn';
jest.mock('./sum.fn');
import toNumArray from './sum.toNumArray';
jest.mock('./sum.toNumArray');

import sumApp from './sum.app';

// INTEGRATION TEST

describe('sum app integration', () => {
  const app = express(); // note: unbound
  beforeAll(async () => await sumApp(app, '/'));

  const toNumArrayMock = toNumArray as jest.Mock;
  const sumFnMock = sumFn as jest.Mock;

  beforeEach(() => {
    jest.resetModules();
  });

  test('good request', async () => {
    toNumArrayMock.mockReturnValueOnce([2, 3, 5]);
    sumFnMock.mockReturnValueOnce(42);
    const resp = await request(app).get('/?nums=xxx');
    expect(toNumArrayMock).toHaveBeenCalledWith('xxx', '/');
    expect(sumFnMock).toHaveBeenCalledWith(2, 3, 5);
    expect(resp.ok).toBe(true);
    expect(resp.body).toMatchObject({ sum: 42 });
  });

  test('toNumArray throws', async () => {
    toNumArrayMock.mockImplementationOnce(() => {
      throw 'bad';
    });
    const resp = await request(app).get('/?nums=xxx');
    expect(toNumArrayMock).toHaveBeenCalledWith('xxx', '/');
    expect(sumFnMock).toHaveBeenCalledTimes(0);
    expect(resp.ok).toBe(false);
    expect(resp.text).toMatch(/bad/);
  });
});
