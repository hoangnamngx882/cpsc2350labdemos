import { Application, Request, Response } from 'express';

import axios from 'axios';
import xml2js from 'xml2js';

const vanWeatherUrl = 'https://weather.gc.ca/rss/city/bc-74_e.xml';

function getWeather(req: Request, res: Response): void {
  // get weather
  axios
    .get(vanWeatherUrl)
    .then(resp => xml2js.parseStringPromise(resp.data))
    .then(feed => feed.feed.entry)
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    .then(entries => Object.values(entries) as { title: string }[])
    .then(entries => entries.map(w => w.title))
    .then(entries => `<ul><li>${entries.join('</li><li>')}</li></ul>`)
    .then(htm => res.status(200).send(htm))
    .catch(err => res.status(500).send(err));
}

function fn(app: Application, path: string): void {
  app.get(path, getWeather);
}

export default async function(
  app: Application,
  path: string
): Promise<Application> {
  return new Promise((resolve /*, reject*/) => {
    fn(app, path);
    resolve(app);
  });
}
