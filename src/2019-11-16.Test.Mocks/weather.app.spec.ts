import request from 'supertest';
import express from 'express';

import axios from 'axios';
jest.mock('axios');
const axiosMock = axios as jest.Mocked<typeof axios>;

import weatherApp from './weather.app';

describe('vanWeather', () => {
  const app = express(); // note: unbound
  beforeAll(async () => await weatherApp(app, '/'));

  beforeEach(() => axiosMock.get.mockReset());

  test('Calls Environment Canada - Vancouver', async () => {
    const expURL = 'https://weather.gc.ca/rss/city/bc-74_e.xml';
    await request(app).get('/');
    // eslint-disable-next-line @typescript-eslint/unbound-method
    expect(axiosMock.get).toHaveBeenCalledWith(expURL);
  });

  test('Good response', async () => {
    const respGood = Promise.resolve({
      data:
        '<feed>\
      <entry><title>current: 1</title></entry>\
      <entry><title>today: 10</title></entry>\
      <entry><title>tonight: 12</title></entry>\
      <entry><title>tomorrow: 42</title></entry>\
      </feed>',
      status: 200,
    });
    axiosMock.get.mockReturnValueOnce(respGood);
    const res = await request(app).get('/');
    expect(res.ok).toBe(true);
    expect(res.text).toMatch(/current: 1/);
    expect(res.text).toMatch(/today: 10/);
    expect(res.text).toMatch(/tonight: 12/);
    expect(res.text).toMatch(/tomorrow: 42/);
  });

  test('Bad response: not xml', async () => {
    const respNotXml = Promise.resolve({ data: 'not xml', status: 200 });
    axiosMock.get.mockReturnValueOnce(respNotXml);
    const res = await request(app).get('/');
    expect(res.ok).toBe(false);
  });
});
