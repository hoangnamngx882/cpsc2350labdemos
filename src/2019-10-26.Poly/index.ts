import { StudentImpl, InternationalStudent, SeniorStudent } from './Student';
import { CourseImpl } from './Course';

function fn(): void {
  const students = [
    new StudentImpl(),
    new InternationalStudent(),
    new SeniorStudent(),
  ];
  students.forEach(s => {
    for (let i = 0; i < 3; i++) {
      const c = new CourseImpl('course' + i, 3);
      s.add(c);
    }
    console.log(s.totalFees);
  });
}

export default async function(): Promise<void> {
  return new Promise((resolve /*, reject*/) => {
    fn();
    resolve();
  });
}
