export default interface List<T> {
  add(e: T): boolean;
  get(index: number): T;
  contains(e: T): boolean;
  indexOf(e: T): number;
}

export class ListImpl<T> {
  private elems: T[] = [];

  get length(): number {
    return this.elems.length;
  }
  add(e: T): boolean {
    this.elems.push(e);
    return true;
  }
  get(index: number): T {
    return this.elems[index];
  }
  contains(e: T): boolean {
    return this.elems.includes(e);
  }
  indexOf(e: T): number {
    return this.elems.indexOf(e);
  }

  protected insert(e: T, index: number): void {
    this.elems.splice(index, 0, e);
  }
}
