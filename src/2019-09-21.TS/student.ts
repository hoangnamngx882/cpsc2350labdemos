import Person from './person';
import Comparable from './comparable';

export default class Student extends Person implements Comparable<number> {
  constructor(name: string, private _gpa?: number) {
    super(name);
  }

  get gpa(): number | undefined {
    return this._gpa;
  }

  compareTo(other: number): 0 | 1 | -1;
  compareTo(other: Person): 0 | 1 | -1;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  compareTo(other: any): 0 | 1 | -1 {
    if (typeof other === 'number') {
      if (this.gpa === undefined) {
        throw 'Cannot compare undefined GPA';
      }
      return Math.sign(this.gpa - other) as 0 | 1 | -1;
    } else if (other instanceof Student) {
      if (other.gpa === undefined) {
        throw 'Cannot compare undefined GPA';
      }
      return this.compareTo(other.gpa);
    } else {
      return super.compareTo(other);
    }
  }
}
