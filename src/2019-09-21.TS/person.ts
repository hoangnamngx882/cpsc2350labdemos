import Comparable from './comparable';

export default class Person implements Comparable<Person> {
  constructor(private readonly _name: string) {}

  get name(): string {
    return this._name;
  }

  compareTo(other: Person): 0 | 1 | -1 {
    return this.name < other.name ? -1 : this.name > other.name ? 1 : 0;
  }
}
