import { Course } from './Course';

export interface Student {
  readonly baseFees: number;
  readonly perCreditFee: number;
  readonly numCredits: number;
  readonly totalFees: number;

  add(c: Course): void;
}

export class StudentImpl implements Student {
  readonly baseFees: number = 200;
  readonly perCreditFee: number = 100;

  private _numCredits = 0;

  get numCredits(): number {
    return this._numCredits;
  }

  get totalFees(): number {
    return this.baseFees + this.perCreditFee * this.numCredits;
  }

  add(c: Course): void {
    this._numCredits += c.credits;
  }
}

export class InternationalStudent extends StudentImpl {
  readonly baseFees: number = 250;
  readonly perCreditFee: number = 590;
}

export class SeniorStudent extends StudentImpl {
  readonly perCreditFee: number = 0;
}
