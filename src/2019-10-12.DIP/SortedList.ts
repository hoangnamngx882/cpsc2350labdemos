import { ListImpl } from './List';

type Comparator<T> = (a: T, b: T) => 0 | -1 | 1;

export default class SortedList<T> extends ListImpl<T> {
  constructor(private readonly comp: Comparator<T>) {
    super();
  }

  add(e: T): boolean {
    const i = this.findInsertPosition(e);
    this.insert(e, i);
    return true;
  }

  private findInsertPosition(e: T): number {
    for (let i = 0; i < this.length; i++) {
      const elem = this.get(i);
      const order = this.comp(e, elem);
      if (order < 0) {
        return i;
      }
    }
    return this.length;
  }
}

export function makeComparator<T>(fn: (a: T, b: T) => number): Comparator<T> {
  return function(a: T, b: T): 0 | -1 | 1 {
    const retVal = fn(a, b);
    return retVal < 0 ? -1 : retVal > 0 ? 1 : 0;
  };
}
