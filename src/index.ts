import express from 'express';

import availableRoutes from './availableRoutes';

/* eslint-disable @typescript-eslint/no-unused-vars */
import sumApp from './sum/sum.app';
import ts from './2019-09-21.TS/index';
import dip from './2019-10-12.DIP/index';
import poly from './2019-10-26.Poly/index';
import weatherApp from './2019-11-16.Test.Mocks/weather.app';
/* eslint-enable @typescript-eslint/no-unused-vars */

const app = express();

Promise.all([sumApp(app, '/sum'), weatherApp(app, '/weather')])
  .then(() => {
    console.log(availableRoutes(app));
    const server = app.listen(process.env.PORT || 8080);
    server.on('listening', () => {
      console.log(server.address());
    });
  })
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  .catch((err: any) => {
    console.error(err);
  });
