import { ListImpl } from './List';
import Student from './Student';

export default class SortedStudentListByName extends ListImpl<Student> {
  add(e: Student): boolean {
    const i = this.findInsertPosition(e);
    this.insert(e, i);
    return true;
  }

  private findInsertPosition(e: Student): number {
    for (let i = 0; i < this.length; i++) {
      const elem = this.get(i);
      const order = e.name.localeCompare(elem.name);
      if (order < 0) {
        return i;
      }
    }
    return this.length;
  }
}
