export default class SortedList {
  private list: number[] = [];

  get length(): number {
    return this.list.length;
  }

  insert(x: number): number {
    const i = this.findPos(x);
    this.list.splice(i, 0, x);
    return i;
  }

  get(i: number): number {
    return this.list[i];
  }

  /**
   * Searches the list for the given element.
   * @param n - the number to find
   * @return if the number is in the list, the index corresponding to the location of the number in the list, otherwise undefined
   */
  find(n: number): number | undefined {
    const i = this.findPos(n);
    return i < this.length && this.list[i] === n ? i : undefined;
  }

  private findPos(x: number): number {
    for (let i = 0; i < this.length; i++) {
      if (this.get(i) >= x) {
        return i;
      }
    }
    return this.length;
  }
}
