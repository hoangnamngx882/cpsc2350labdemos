export default interface Comparable<T> {
  compareTo(other: T): 0 | 1 | -1;
}
