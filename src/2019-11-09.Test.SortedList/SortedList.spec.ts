import SortedList from './SortedList';

describe('SortedList', () => {
  let sl: SortedList;

  beforeEach(() => {
    sl = new SortedList();
    sl.insert(2);
    sl.insert(5);
    sl.insert(7);
  });

  test('init', () => {
    sl = new SortedList();
    expect(sl).toHaveLength(0);
  });

  describe('insert', () => {
    test('one element', () => {
      sl = new SortedList();
      expect(sl.insert(5)).toBe(0);
      expect(sl).toHaveLength(1);
    });

    describe('more than one element', () => {
      ([
        ['at front', 1, 0],
        ['at end', 8, 3],
        ['in middle', 5, 1],
      ] as [string, number, number][]).forEach(([name, toInsert, expIndex]) => {
        test(name, () => {
          expect(sl.insert(toInsert)).toBe(expIndex);
          expect(sl).toHaveLength(4);
        });
      });
    });
  }); // insert

  describe('get', () => {
    test('at front', () => {
      expect(sl.get(0)).toBe(2);
    });
    test('at end', () => {
      expect(sl.get(1)).toBe(5);
    });
    test('in middle', () => {
      expect(sl.get(2)).toBe(7);
    });
  }); // get

  describe('find', () => {
    test('not there', () => {
      expect(sl.find(6)).not.toBeDefined();
    });
    test('at front', () => {
      expect(sl.find(2)).toBe(0);
    });
    test('at end', () => {
      expect(sl.find(5)).toBe(1);
    });
    test('in middle', () => {
      expect(sl.find(7)).toBe(2);
    });
  });
});
