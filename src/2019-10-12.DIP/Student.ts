export default interface Student {
  readonly name: string;
  readonly studentID: number;
}

export class StudentImpl implements Student {
  constructor(
    private readonly _name: string,
    private readonly _studno: number
  ) {}

  get name(): string {
    return this._name;
  }

  get studentID(): number {
    return this._studno;
  }
}
