import SortedList, { makeComparator } from './SortedList';
import Student, { StudentImpl } from './Student';

function fn(): void {
  const compByName = makeComparator((a: Student, b: Student) =>
    a.name.localeCompare(b.name)
  );
  const compByID = makeComparator(
    (a: Student, b: Student) => a.studentID - b.studentID
  );

  const unsorted = [
    new StudentImpl('Alice', 1010),
    new StudentImpl('Bob', 1001),
    new StudentImpl('Cloe', 1005),
  ];

  const listOfStudentsByName = new SortedList<Student>(compByName);
  const listOfStudentsByID = new SortedList<Student>(compByID);

  for (const student of unsorted) {
    listOfStudentsByName.add(student);
    listOfStudentsByID.add(student);
  }

  console.log('              unsort:', unsorted);
  console.log('listOfStudentsByName:', listOfStudentsByName);
  console.log('  listOfStudentsByID:', listOfStudentsByID);
}

export default async function(): Promise<void> {
  return new Promise((resolve /*, reject*/) => {
    fn();
    resolve();
  });
}
