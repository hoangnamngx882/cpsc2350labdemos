In-lab demos from [CPSC 2350](https://langara.ca/programs-and-courses/courses/CPSC/2350.html).

## Installation
```sh
git clone https://gitlab.com/jhilliker/cpsc2350labdemos.git
cd cpsc2350labdemos
npm install
```
Run with: ```npm run dev```

## nodeproject set-up script

```sh
curl -L https://bit.ly/2MaMetV | sh -s
```
Run with: ```nodeproject [dir]```
