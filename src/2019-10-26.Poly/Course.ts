export interface Course {
  readonly name: string;
  readonly credits: number;
}

export class CourseImpl implements Course {
  constructor(readonly name: string, readonly credits: number) {}
}
