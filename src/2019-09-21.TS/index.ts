import Person from './person';
import Student from './student';

function fn(): void {
  const alice = new Person('Alice');
  const bob = new Person('Bob');

  console.log('A.B', alice.compareTo(bob)); // -1
  console.log('B.A', bob.compareTo(alice)); // 1

  const cloe = new Student('Cloe', 3.8);
  const dan = new Person('Dan');
  const ed = new Student('Ed', 3.5);

  console.log('D.C', dan.compareTo(cloe)); // 1
  console.log('D.E', dan.compareTo(ed)); // -1
  console.log('C.E', cloe.compareTo(ed)); // 1

  console.log('C.3', cloe.compareTo(3)); // 1
  console.log('C.4', cloe.compareTo(4)); // -1

  const fred = new Student('Fred');
  console.log('C.F', cloe.compareTo(fred)); // throws
}

export default async function(): Promise<void> {
  return new Promise((resolve /*, reject*/) => {
    fn();
    resolve();
  });
}
