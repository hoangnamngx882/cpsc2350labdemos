import { Application } from 'express';

/* eslint-disable @typescript-eslint/no-explicit-any */

// https://stackoverflow.com/questions/14934452/how-to-get-all-registered-routes-in-express
export default function availableRoutes(
  app: Application
): { method: string; path: string }[] {
  return app._router.stack
    .filter((r: any) => r.route)
    .map((r: any) => {
      return {
        method: Object.keys(r.route.methods)[0].toUpperCase(),
        path: r.route.path,
      };
    });
}
